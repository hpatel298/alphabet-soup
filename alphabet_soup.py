filename = 'input.txt'
#filename = 'input2.txt'

'''
Recursive function to search if the word is in the grid.
Returns None if the word is not found.
Returns location of the last character's coordinates if the word is found in the grid.
'''
def is_word_in_grid(grid, word, row, col, row_offset, col_offset, rows, cols):
    # increment row and col based on offsets passed in
    row = row + row_offset
    col = col + col_offset

    # if out of range of the grid, word is not found
    if (row < 0 or col < 0 or row > rows - 1 or col > cols - 1):
        return None
    
    # if last character matches, word is found, return coordinates
    if (len(word) == 1 and grid[row][col] == word):
        return row, col
    
    # if current character matches, recursively call the function by passing rest of the word
    if (len(word) > 1 and grid[row][col] == word[0]):
        return is_word_in_grid(grid, word[1:len(word)], row, col, row_offset, col_offset, rows, cols)
    else:
        return None


# Open input file and read all lines
with open(filename) as f:
    lines =  f.read().splitlines() 

# Get grid dimensions
grid_dims = lines[0].split('x')
rows = int(grid_dims[0])
cols = int(grid_dims[1])

# Create grid
grid = []
for row in range(rows):
    grid.append(lines[row+1].split(' '))

# Read words from the bottom of the grid
words = []
for w in range(rows+1, len(lines)):
    words.append(lines[w])

# Find all possible matches for each word. We look for first character of the word in the grid
# and find all possibilities and note the coordinates.  
possibilities = []
for word in words:
    for row in range(rows):
        for col in range(cols):
            if (grid[row][col] == word[0]):
                possibilities.append([word, row, col])

# The search directions are defines as offsets from the current position in the grid
offsets = [[0, 1],        # Horizontal right
           [0, -1],       # Horizontal left
           [1, 0],        # Vertical down
           [-1, 0],       # Vertical up
           [1, 1],        # Diagonal right down
           [1, -1],       # Diagonal left down
           [-1, 1],       # Diagonal right up
           [-1, -1]]      # Diagonal Left up

# For every word match possibility, search in all directions as defined by the offsets
results = []
for possibility in possibilities:
    for offset in offsets:
        last_index = is_word_in_grid(grid, possibility[0][1:len(possibility[0])], possibility[1], possibility[2], offset[0], offset[1], rows, cols)
        if last_index is not None:
            results.append([possibility[0], possibility[1], possibility[2], last_index[0], last_index[1]])

# Print results as requested
for result in results:
    print('{0} {1}:{2} {3}:{4}'.format(result[0], result[1], result[2], result[3], result[4]))